﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class TrackingManager : MonoBehaviour
{
	#region Dependencies

	// Singleton instance
	public static TrackingManager instance;

	// Reference to AR Foundation Scripts
	private ARTrackedImageManager ARTrackedImageManager = null;
	private ARSessionOrigin AROrigin_ref = null;

	// Dynamic Dictionary of Trackers, constructed with Trackers components
	protected Dictionary<string, Tracker> trackedObjects = null;

	// Dynamic Dictionary of static objects already tracked (STATIC)
	protected Dictionary<string, Tracker> staticObjects = null;

	// Dynamic Dictionary of dynamic objects already tracked (DYNAMIC)
	protected Dictionary<string, Tracker> dynamicObjects = null;

	// Dynamic Dictionary of objects requiring a timeout
	protected Dictionary<string, float> timedoutObjects = null;

	// Dynamic Dictionary of objects timed out, waiting for new behaviour
	protected Dictionary<string, Tracker> alreadyTimedoutObjects = null;

	// Dynamic Dictionary of dynamic tracked image to keep transform on calculated image position
	protected Dictionary<string, ARTrackedImage> dynamicTrackedImages = null;

	// Dynamic Dictionnary of bounds collection foreach dynamic object
	protected Dictionary<string, Bounds> dynamicObjectsBounds = null;

	// Dynamic Dictionnary of bounds collection foreach dynamic object
	protected Dictionary<string, ARTrackedImage> correctedPosition = null;

	[Tooltip("Minimum frame count for tracking checker")]
	[SerializeField]
	protected float mimimunNumberOfFrames = 40;

	[Tooltip("Minimum distance for tracking checker")]
	[SerializeField]
	protected float maximumFrameDistance = 0.005f;

	[Tooltip("Security length before to track again")]
	[SerializeField]
	protected float securityTime = 3f;

	[Tooltip("Minimum distance to trigger ajustment on position")]
	[SerializeField]
	protected float minPositionDistance = 0.5f;

	[Tooltip("Maximum distance to achieve to correct position again")]
	[SerializeField]
	protected float maxPositionDistance = 1f;

	[Tooltip("Minimum lerp time to ajust the position")]
	[SerializeField]
	protected float minPositionTime = 2f;

	[Tooltip("Maximum lerp time to ajust the position")]
	[SerializeField]
	protected float maxPositionTime = 10f;

	[Tooltip("Lerping time for correcting position")]
	[SerializeField]
	protected float correctingTime = 3f;

	// Currently tracking
	private bool TrackingON = false;

	// Currently correcting position
	private bool correctingON = false;

	[Tooltip("If True, at least one static tracker was called. Else, return false")]
	[SerializeField]
	private bool correctPositionReady = false;

	// Reference to current Tracked Image
	private ARTrackedImage currentTrackedImage = null;

	[Tooltip("Prefab used for reset ARSessionOrigin object")]
	[SerializeField]
	private GameObject ARSessionOrigin = null;

	public float debugDistance = 0.005f;
	public float debugTime = 20f;
	public float debugSpeed = 1f;

	#endregion Dependencies

	#region MonoBehaviour

	void Awake()
	{
		Initialize();
	}

	// Start is called before the first frame update
	void Start()
	{
		TrackingON = true;
		LaunchSecurity();
	}

	// Initialize references and dictionaries
	public void Initialize()
	{
		instance = this;

		trackedObjects = new Dictionary<string, Tracker>();
		staticObjects = new Dictionary<string, Tracker>();
		dynamicObjects = new Dictionary<string, Tracker>();
		timedoutObjects = new Dictionary<string, float>();
		alreadyTimedoutObjects = new Dictionary<string, Tracker>();
		dynamicTrackedImages = new Dictionary<string, ARTrackedImage>();
		dynamicObjectsBounds = new Dictionary<string, Bounds>();
		correctedPosition = new Dictionary<string, ARTrackedImage>();


		ARTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
		AROrigin_ref = FindObjectOfType<ARSessionOrigin>();
	}

	// Update is called once per frame
	void Update()
	{
		CheckTrackedImage();
		UpdateRendering();
		UpdateTimeOut();
		UpdateCorrectedPosition();
	}

	// Image change reference, needed for Dynamic Tracking
	private void OnEnable()
	{
		ARTrackedImageManager.trackedImagesChanged += ImageChanged;
	}

	// Image change reference, needed for Dynamic Tracking
	private void OnDisable()
	{
		ARTrackedImageManager.trackedImagesChanged -= ImageChanged;
	}

	#endregion MonoBehaviour

	#region Tracking

	#region Functions

	// This function is used when reseting references before Destroy/Instantiate ARSessionsOrigin prefab
	public void ResetAllReferences()
	{
		trackedObjects = new Dictionary<string, Tracker>();
		staticObjects = new Dictionary<string, Tracker>();
		dynamicObjects = new Dictionary<string, Tracker>();
		timedoutObjects = new Dictionary<string, float>();
		alreadyTimedoutObjects = new Dictionary<string, Tracker>();
		dynamicTrackedImages = new Dictionary<string, ARTrackedImage>();
		dynamicObjectsBounds = new Dictionary<string, Bounds>();
		correctedPosition = new Dictionary<string, ARTrackedImage>();


		ARTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
		AROrigin_ref = FindObjectOfType<ARSessionOrigin>();
	}

	// Check the corrected position list to remove trackers being over maxPositionDistance from camera
	public void UpdateCorrectedPosition()
	{
		List<string> toRemove = new List<string>();
		foreach (KeyValuePair<string, ARTrackedImage> item in correctedPosition)
		{
			if (Vector3.Distance(Camera.main.transform.position, item.Value.transform.position) >= maxPositionDistance)
			{
				toRemove.Add(item.Key);
			}
		}
		foreach (string key in toRemove)
		{
			correctedPosition.Remove(key);
		}
	}

	// Update timers for objects presents in timedoutObjects
	public void UpdateTimeOut()
	{
		foreach (KeyValuePair<string, Tracker> valuePair in dynamicObjects)
		{
			// Update time out
			if (timedoutObjects.ContainsKey(valuePair.Key))
			{
				if (timedoutObjects[valuePair.Key] > 0f)
				{
					timedoutObjects[valuePair.Key] -= Time.deltaTime;
				}
				else
				{
					TimeOut(valuePair.Key);
				}
			}
		}
	}

	// Update rendering for dynamicobjects, if StopRenderStatus option is setted
	public void UpdateRendering()
	{
		foreach (KeyValuePair<string, Tracker> valuePair in dynamicObjects)
		{
			Tracker TD = valuePair.Value;
			if (dynamicTrackedImages.ContainsKey(valuePair.Key) && TD.GetStopRenderStatus())
			{
				if (!TrackingOk(dynamicTrackedImages[valuePair.Key]) && TD.GetRendering())
				{
					TD.ChangeRendering(false);
					TD.SetRendering(false);
				}
				else if (TrackingOk(dynamicTrackedImages[valuePair.Key]) && !TD.GetRendering())
				{
					TD.ChangeRendering(true);
					TD.SetRendering(true);
				}
			}
		}
	}

	// Generic function to feed Tracker definition
	// This function is NEVER called from this script but from component Tracker
	public void AddTrackedObject(Tracker T)
	{
		// If Tracker not already present in list
		if (!TrackerInDictionay(T.GetImageName(), trackedObjects))
		{
			// Constructed as copy
			Tracker copy = new Tracker(T);

			CanvasManager.instance.DebugLine("Tracker " + T.GetTrackingMode() + " with name  " + T.GetImageName() + " was added " + "Correct position ? " + T.GetCorrectingPosition() + " scene load ? " + T.GetSceneLoading());
			CanvasManager.instance.DebugLine(T.GetLoadingMode() + " mode on " + T.GetScenesToLoad().Length + "scenes to load and " + T.GetSceneToUnload().Length + " scenes to unload");
			CanvasManager.instance.DebugLine("Use instantiation ? " + T.GetInstantiated() + ", interact on " + T.GetObjectToMove() + " and will timout after > 0s : " + T.GetTimeOut() + ", is this definitive " + T.GetTimeOutStatus());
			CanvasManager.instance.DebugLine("necessary to destroy associated GO " + T.GetDestroyStatus() + " Stop rendering at lost : " + T.GetStopRenderStatus());
			CanvasManager.instance.DebugLine("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");

			// Add to list
			trackedObjects.Add(copy.GetImageName(), copy);
		}
	}

	// Generic function to know if a Dictionary Dict contains a key named "name"
	public bool TrackerInDictionay(string name, Dictionary<string, Tracker> Dict)
	{
		return Dict.ContainsKey(name);
	}

	// Image change behaviour, called on added, updated and removed trackers
	public void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
	{
		foreach (ARTrackedImage trackedImage in eventArgs.added)
		{
			UpdateImage(trackedImage);
		}

		foreach (ARTrackedImage trackedImage in eventArgs.updated)
		{
			UpdateImage(trackedImage);
		}

		foreach (ARTrackedImage trackedImage in eventArgs.removed)
		{
			CanvasManager.instance.DebugLine("Tracked Image " + trackedImage.name + " was removed at " + Time.deltaTime);
		}
	}

	// Search for Tracked Image
	private void CheckTrackedImage()
	{
		// Check if tracked image detected
		ARTrackedImage image = null;
		image = FindObjectOfType<ARTrackedImage>();

		// if image exists, traking is currently off and tracking state is ok
		if (image != null && !TrackingON && !image.trackingState.Equals(TrackingState.None))
		{
			string imageName = image.referenceImage.name;
			// Check that image is not ignored, security timer is under or equal 0 and there is no scene actually loading
			if (!TrackerInDictionay(imageName, staticObjects) && !TrackerInDictionay(imageName, dynamicObjects))
			{
				currentTrackedImage = image;
				TrackingON = true;
				CanvasManager.instance.DebugLine("Image found with name : " + image.referenceImage.name + " Starting coroutine at" + Time.frameCount);
				StartCoroutine(Tracking(trackedObjects[imageName]));
			}
		}
	}

	public static Bounds getBounds(GameObject obj)
	{
		Bounds bounds = new Bounds();
		Renderer[] renderers = obj.GetComponentsInChildren<Renderer>();
		if (renderers.Length > 0)
		{
			//Find first enabled renderer to start encapsulate from it

			foreach (Renderer renderer in renderers)
			{
				if (renderer.enabled)
				{
					bounds = renderer.bounds;
					break;
				}
			}

			//Encapsulate for all renderers
			foreach (Renderer renderer in renderers)
			{
				if (renderer.enabled)
				{
					bounds.Encapsulate(renderer.bounds);
				}
			}
		}
		return bounds;
	}

	// Tracking state okay
	private bool TrackingOk(ARTrackedImage image)
	{
		Bounds globalBounds = dynamicObjectsBounds[image.referenceImage.name];
		List<Vector3> extremities = new List<Vector3>();

		Vector3 boundPoint1 = globalBounds.min;
		Vector3 boundPoint2 = globalBounds.max;
		Vector3 boundPoint3 = new Vector3(boundPoint1.x, boundPoint1.y, boundPoint2.z);
		Vector3 boundPoint4 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint1.z);
		Vector3 boundPoint5 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint1.z);
		Vector3 boundPoint6 = new Vector3(boundPoint1.x, boundPoint2.y, boundPoint2.z);
		Vector3 boundPoint7 = new Vector3(boundPoint2.x, boundPoint1.y, boundPoint2.z);
		Vector3 boundPoint8 = new Vector3(boundPoint2.x, boundPoint2.y, boundPoint1.z);

		extremities.Add(boundPoint1);
		extremities.Add(boundPoint2);
		extremities.Add(boundPoint3);
		extremities.Add(boundPoint4);
		extremities.Add(boundPoint5);
		extremities.Add(boundPoint6);
		extremities.Add(boundPoint7);
		extremities.Add(boundPoint8);

		Transform imageT = image.transform;
		foreach (Vector3 pos in extremities)
		{
			Vector3 imagePos = imageT.TransformPoint(pos);
			Vector3 projectedImagePos = Camera.main.WorldToScreenPoint(imagePos);

			// Check if screen point coordinates for imagePos is ok
			bool axisXOk = (projectedImagePos.x >= 0f && projectedImagePos.x <= Screen.width);
			bool axisYOk = (projectedImagePos.y >= 0f && projectedImagePos.y <= Screen.height);

			// If Screen point coordinates of image is inside
			if (axisXOk && axisYOk)
			{
				return true;
			}
		}
		return false;
	}

	// Update transform for continuous tracking
	private void UpdateImage(ARTrackedImage trackedImage)
	{
		string name = trackedImage.referenceImage.name;

		Tracker TD = null;

		// Update position for dynamic objects
		if (TrackerInDictionay(name, dynamicObjects))
		{
			TD = dynamicObjects[name];
			TD.UpdateObject(trackedImage.transform);
			
			// If Tracker is dynamic, exit UpdateImage know, no need to check for static
			return;
		}
		else if(TrackerInDictionay(name,trackedObjects))
		{
			TD = trackedObjects[name];

			// Check distance for position correction (Statics)
			float dist = Vector3.Distance(Camera.main.transform.position, trackedImage.transform.position);

			// Does this static tracker is setted for correct position
			bool correct = TD.GetCorrectingPosition();

			// Does the tracking state is ok
			bool trackingStateOk = !trackedImage.trackingState.Equals(TrackingState.None);

			// Tracker not already corrected and to close
			bool alreadyCorrected = correctedPosition.ContainsKey(name);


			// Update position for dynamic objects
			if (staticObjects.ContainsKey(name) && dist <= minPositionDistance && !correctingON && !alreadyCorrected && trackingStateOk && correct)
			{
				CanvasManager.instance.DebugLine("Calling correct position from image update");
				CallSmoothChange(staticObjects[name].GetObjectToMove(), trackedImage.transform);
			}

		}

	}

	// Call custom load routine
	public void CallCustomLoad(string[] scene, LoadSceneMode sceneMode, string[] sceneToUnload = null, bool fading = true)
	{
		CanvasManager.instance.DebugLine("Custom load called");
		StartCoroutine(CustomLoad(scene, sceneMode, sceneToUnload,fading));
	}

	// Time out behaviour
	public void TimeOut(string name)
	{
		if (dynamicObjects.ContainsKey(name))
		{
			bool forever = dynamicObjects[name].GetTimeOutStatus();
			bool destroy = dynamicObjects[name].GetDestroyStatus();

			if (forever)
			{
				if (destroy)
				{
					Destroy(dynamicObjects[name].GetObjectToMove());
				}
				timedoutObjects.Remove(name);
				dynamicObjects.Remove(name);
				dynamicTrackedImages.Remove(name);
				dynamicObjectsBounds.Remove(name);
				trackedObjects.Remove(name);
			}
			else
			{
				// Get the tracker
				Tracker KVP = dynamicObjects[name];
				// Disable Object
				dynamicObjects[name].GetObjectToMove().SetActive(false);

				// Removing from references
				timedoutObjects.Remove(name);
				dynamicObjects.Remove(name);
				dynamicTrackedImages.Remove(name);
				dynamicObjectsBounds.Remove(name);
				trackedObjects.Remove(name);

				// Adding to already time out objects.
				// Use this list to assign it a new behaviour
				alreadyTimedoutObjects.Add(name, KVP);
			}

			CanvasManager.instance.DebugWarning("Tracker " + name + " time out at " + Time.frameCount + " forever ? " + forever + " and destroy object ? " + destroy);

		}
		else
		{
			CanvasManager.instance.DebugWarning("Tracker no present in dynamicObjects, cannot time it out");
		}
	}

	// Call correction position
	public void CallSmoothChange(GameObject Go, Transform image,Tracker T = null)
	{
		ARTrackedImage trackedImage = image.GetComponent<ARTrackedImage>();
		correctedPosition.Add(trackedImage.referenceImage.name, trackedImage);
		correctingON = true;

		if(T!= null && T.GetUseOffset())
		{
			StartCoroutine(SmoothOriginChange(Go, image,T));

		}
		else
		{
			StartCoroutine(SmoothOriginChange(Go, image));

		}

	}

	// Call security routine
	public void LaunchSecurity()
	{
		StartCoroutine(Security());
	}

	// Function to know if Scene S is currently loaded
	public bool SceneLoaded(string S)
	{
		for (int i = SceneManager.sceneCount-1; i >= 0; i--)
		{
			if (SceneManager.GetSceneAt(i).name.Equals(S))
			{
				return true;
			}
		}
		return false;
	}

	#endregion Functions

	#region Coroutines

	// Tracking behaviour, depends of Tracker role and mode
	public IEnumerator Tracking(Tracker T)
	{
		CanvasManager.instance.DebugLine("Starting tracking");

		// Name of tracked image
		string imageName = T.GetImageName();

		GameObject ObjectToAppear = T.GetObjectToMove();

		bool UseOffset = T.GetUseOffset();

		Vector3 PositionOffset = Vector3.zero;
		Quaternion RotationOffset = Quaternion.identity;

		if(UseOffset)
		{
			PositionOffset = T.GetPositionOffset();
			RotationOffset = T.GetRotationOffset();
		}

		// Depending Tracking mode
		switch (T.GetTrackingMode())
		{
			case Tracker.TrackingMode.Static:
			{
				CanvasManager.instance.DebugLine("Entering in static tracker behaviour");
				CanvasManager.instance.SetTrackingPanel(true);

				// Loop condition
				bool stable = false;

				// Distance for analysis
				float distanceDifferential = 0;

				// Ok frames, distance under minimal threshold
				int stableFrames = 0;

				// Store initial position, and update this Vector3 over time
				Vector3 comparePosition = currentTrackedImage.transform.position;

				while (!stable)
				{
					// Update UI ratio for tracking indicator
					CanvasManager.instance.RefreshRatio((float)stableFrames / (float)mimimunNumberOfFrames);

					// Store distance differential
					distanceDifferential = Vector3.Distance(comparePosition, currentTrackedImage.transform.position);

					// Check compare to minimal distance
					if (distanceDifferential < maximumFrameDistance)
					{
						stableFrames++; // Distance ok, keep going
					}
					else
					{
						stableFrames = 0; // Distance changed, reset count
					}

					// If number of Ok frames is correct, we can trigger associated behaviour
					if (stableFrames >= mimimunNumberOfFrames && !stable)
					{
						// ending loop security, even if break is called
						stable = true;

						// Is this tracker does not trigger scene loading ? 
						if (!T.GetSceneLoading())
						{
							// Is this tracker trigger instantiate a object 
							if (T.GetInstantiated())
							{
								GameObject Go = Instantiate(ObjectToAppear, null);
								Go.transform.position = currentTrackedImage.transform.position + PositionOffset;
								Go.transform.rotation = currentTrackedImage.transform.rotation * RotationOffset;
								T.SetInstanceForObject(Go);
							}
							else
							{
								// This tracker use an existing object, Is this affect position ? 
								if (T.GetCorrectingPosition())
								{
									// At least one call to MakeContentAppear was done
									if (correctPositionReady)
									{
										CallSmoothChange(ObjectToAppear, currentTrackedImage.transform,T);
									}
									else // First call, set correctPositionReady to true for next one
									{
										correctedPosition.Add(imageName, currentTrackedImage);

										ObjectToAppear.transform.SetPositionAndRotation(currentTrackedImage.transform.position + PositionOffset, currentTrackedImage.transform.rotation*RotationOffset);

										if (!correctPositionReady)
										{
											correctPositionReady = true;
										}
									}
								}
								else
								{
									// Change ARSessionOrigin transform to make content appears at the correct place
									//AROrigin_ref.MakeContentAppearAt(ObjectToAppear.transform, currentTrackedImage.transform.position, currentTrackedImage.transform.rotation);
									ObjectToAppear.transform.SetPositionAndRotation(currentTrackedImage.transform.position+PositionOffset, currentTrackedImage.transform.rotation*RotationOffset);

								}
							}
						}
						else // This tracker trigger a scene loading
						{
							if (T.GetSceneToUnload().Length > 0)
							{
								CanvasManager.instance.DebugLine("Calling load coroutine WITH unload at : " + Time.frameCount);
								CallCustomLoad(T.GetScenesToLoad(), T.GetLoadingMode(), T.GetSceneToUnload(),T.GetFading());
							}
							else
							{
								CanvasManager.instance.DebugLine("Calling load coroutine WITHOUT unload at : " + Time.frameCount);
								CallCustomLoad(T.GetScenesToLoad(), T.GetLoadingMode(),null,T.GetFading());
							}
						}

						break;
					}
					// Update position for analysis
					comparePosition = currentTrackedImage.transform.position;
					yield return new WaitForEndOfFrame();
				}

				yield return new WaitForEndOfFrame();

				if (ObjectToAppear!=null)
				{
					CanvasManager.instance.DebugLine("Object : " + ObjectToAppear.name + " is placed at : " + ObjectToAppear.transform.position);
				}

				// Add Tracker and image name to staticObjects dictionary
				if(!T.GetSceneLoading())
				{
					staticObjects.Add(imageName, T);

				}

				// Disable tracking panel
				CanvasManager.instance.SetTrackingPanel(false);

				CanvasManager.instance.DebugLine("Exit static tracker behaviour");

				break;
			}

			case Tracker.TrackingMode.Dynamic:
			{
				CanvasManager.instance.DebugLine("Entering in dynamic tracker behaviour");

				// Is this tracker does not trigger scene loading ? 
				if (!T.GetSceneLoading())
				{
					// Is this tracker trigger instantiate a object 
					if (T.GetInstantiated())
					{
						GameObject Go = Instantiate(ObjectToAppear, null);
						Go.transform.position = currentTrackedImage.transform.position;
						Go.transform.rotation = currentTrackedImage.transform.rotation;
						T.SetInstanceForObject(Go);

					}
					else
					{
						// Change ARSessionOrigin transform to make content appears at the correct place
						//AROrigin_ref.MakeContentAppearAt(ObjectToAppear.transform, currentTrackedImage.transform.position, currentTrackedImage.transform.rotation);
						ObjectToAppear.transform.SetPositionAndRotation(currentTrackedImage.transform.position, currentTrackedImage.transform.rotation);

					}

					if (T.GetTimeOut() > 0)
					{
						timedoutObjects.Add(imageName, T.GetTimeOut());
						CanvasManager.instance.DebugLine("Tracker " + T.GetImageName() + " will time out in " + T.GetTimeOut() + " seconds");
					}

					

					dynamicObjects.Add(imageName, T);
					dynamicTrackedImages.Add(imageName, currentTrackedImage);

					if (ObjectToAppear != null)
					{
						dynamicObjectsBounds.Add(imageName, getBounds(ObjectToAppear));
					}
				}
				else // This tracker trigger a scene loading
				{
					CallCustomLoad(T.GetScenesToLoad(), T.GetLoadingMode(),T.GetSceneToUnload(),T.GetFading());
				}


				CanvasManager.instance.DebugLine("Exit dynamic tracker behaviour");

				break;
			}
		}

		// Reset tracked image reference
		currentTrackedImage = null;

		// Launch security

		LaunchSecurity();
		

		CanvasManager.instance.DebugLine("Exit Tracking Routine at frame " + Time.frameCount);

		yield return null;
	}

	// Security coroutine, used to prevent multiple call
	public IEnumerator Security()
	{
		// Wait realtime on "securityTime" seconds
		yield return new WaitForSecondsRealtime(securityTime);

		// End of security
		TrackingON = false;

		yield return null;
	}

	// Unique load rootine, will load specified scene and remove scenes from ScenesToUnload
	public IEnumerator CustomLoad(string[] scenesToLoad, LoadSceneMode sceneMod, string[] scenesToUnload = null, bool fade = true)
	{
		if(fade)
		{
			CanvasManager.instance.Fade(true);

			CanvasManager.instance.DebugLine("Loading triggered, wait for fade time");

			// Wait fade length
			yield return new WaitForSecondsRealtime(CanvasManager.instance.GetFadeTime());
		}



		// If loadscene is additive, we need to reset our ARTrackedManager
		if (sceneMod.Equals(LoadSceneMode.Additive))
		{
			CanvasManager.instance.DebugLine("unload for additive");

			// Reset all references || might be intersting to set it as parameter for Tracker
			ResetAllReferences();

			CanvasManager.instance.DebugLine("reset reference");


			yield return new WaitForEndOfFrame();

			// Keep values for origin transform
			Transform origin = GameObject.FindObjectOfType<ARSessionOrigin>().transform;

			Vector3 pos = origin.position;
			Quaternion rot = origin.rotation;
			Vector3 scale = origin.localScale;

			yield return new WaitForEndOfFrame();
			CanvasManager.instance.DebugLine("keep transform infos");

			// Destroy ARSessionOrigin gameObject
			Destroy(GameObject.FindObjectOfType<ARSessionOrigin>().gameObject);
			CanvasManager.instance.DebugLine("Destroying old ARSO");

			// Wait a fixed time
			yield return new WaitForSecondsRealtime(0.1f);

			// Instantiate new AR Session Origin
			GameObject ARsession = Instantiate(ARSessionOrigin, null);
			ARsession.name = "ARSessionOrigin";

			yield return new WaitForSecondsRealtime(0.1f);

			// Affect stored transform values for origin
			ARsession.transform.position = pos;
			ARsession.transform.rotation = rot;
			ARsession.transform.localScale = scale;

			CanvasManager.instance.DebugLine("Instantiate a new ARSessionOrigin");

			yield return new WaitForSecondsRealtime(0.1f);
		}

		// Loading scene if list length > 0
		if(scenesToLoad != null && scenesToLoad.Length>0)
		{
			CanvasManager.instance.DebugLine("There are scenes to load");

			// If LoadSceneMode is additive, load asynchronously scenes from list
			if (sceneMod.Equals(LoadSceneMode.Additive))
			{
				CanvasManager.instance.DebugLine("Precisely : " + scenesToLoad.Length);

				for (int i = 0; i< scenesToLoad.Length; i++)
				{
					// If scene not null and not already loaded
					if (scenesToLoad[i] != null && !SceneLoaded(scenesToLoad[i]))
					{
						// Create async load operation for this scene
						AsyncOperation loadAsync = SceneManager.LoadSceneAsync(scenesToLoad[i], sceneMod);
						CanvasManager.instance.DebugLine("load for additive scene : " + scenesToLoad[i]);

						// Wait until unload
						while (!loadAsync.isDone)
						{
							yield return new WaitForEndOfFrame();
						}
						CanvasManager.instance.DebugLine("Scene : " + scenesToLoad[i] + " ended to load async");

					}
				}
				CanvasManager.instance.DebugLine("Loading async done");

			}
			else
			{
				CanvasManager.instance.DebugLine("Load single bloc");

				// If scene not null, just load single
				if (scenesToLoad[0]!=null)
				{
					CanvasManager.instance.DebugLine("Load single " + scenesToLoad[0]);
					//yield return new WaitForSecondsRealtime(1f);

					SceneManager.LoadScene(scenesToLoad[0], sceneMod);
				}
				else
				{
					CanvasManager.instance.DebugLine("Scene to load seems to be null");

				}
			}
		}
		else
		{
			CanvasManager.instance.DebugLine("No scenes to load");
		}

		yield return new WaitForEndOfFrame();

		CanvasManager.instance.DebugLine("END OF LOAD PART");


		// Unloading scene if list length > 0
		if (scenesToUnload!=null && scenesToUnload.Length > 0)
		{
			CanvasManager.instance.DebugLine("Start to Unload");

			// Usually, it would always going to if, load simple will kill this script instance
			if (sceneMod.Equals(LoadSceneMode.Additive))
			{
				CanvasManager.instance.DebugLine(scenesToUnload.Length + " scenes to unload");

				for (int i = 0; i < scenesToUnload.Length; i++)
				{

					// If scene not nul and currently loaded
					if (scenesToUnload[i] != null && SceneLoaded(scenesToUnload[i]))
					{
						CanvasManager.instance.DebugLine("unload scene " + scenesToUnload[i]);

						AsyncOperation unloadAsync = SceneManager.UnloadSceneAsync(scenesToUnload[i], UnloadSceneOptions.None);

						while (!unloadAsync.isDone)
						{
							yield return new WaitForEndOfFrame();
						}
					}
				}
			}

			else
			{
				CanvasManager.instance.DebugWarning("If you see this, load coroutine still going to unload block even if LoadSceneMode is additive");
			}
		}
		else
		{
			CanvasManager.instance.DebugLine("No scenes to unload");

		}

		CanvasManager.instance.DebugLine("End of additive loading coroutine");

		if(fade)
		{
			yield return new WaitForSecondsRealtime(CanvasManager.instance.GetFadeTime());
			CanvasManager.instance.Fade(false);
		}
	}

	// Routine to correct position over time, depending of distance between current object and corresponding image position
	public IEnumerator SmoothOriginChange(GameObject Go, Transform image,Tracker T = null)
	{
		float ratio = 0f;
		float timer = 0f;

		Vector3 startPos = Go.transform.position;
		Quaternion startRot = Go.transform.rotation;

		Vector3 PositionOffset = Vector3.zero;
		Quaternion RotationOffset = Quaternion.identity;

		if(T!=null && T.GetUseOffset())
		{
			PositionOffset = T.GetPositionOffset();
			RotationOffset = T.GetRotationOffset();
		}

		while (timer < correctingTime)
		{
			ratio = timer / correctingTime;
			Go.transform.SetPositionAndRotation(Vector3.Lerp(startPos, image.position+ PositionOffset, ratio),Quaternion.Lerp(startRot, image.rotation*RotationOffset, ratio));
			timer += Time.deltaTime;
			yield return null;
		}

		// Final position call, appear at final position
		Go.transform.SetPositionAndRotation(image.position + PositionOffset, image.rotation * RotationOffset);

		correctingON = false;
	}

	#endregion Coroutines

	#endregion Tracking

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

public class SceneLoader : MonoBehaviour
{
	#region Dependencies

	public static SceneLoader instance;

	[Tooltip("ARSession Origin modified prefab")]
	[SerializeField]
	private GameObject ARSessionOrigin;

	[Tooltip("All tracking scenes names")]
	[SerializeField]
	private string[] trackingScenes;

	[Tooltip("First scene to load")]
	[SerializeField]
	private string firstScene = "1_Tracker_fixe";

	[SerializeField]
	private bool isLoading = false;

	#endregion Dependencies

	#region MonoBehaviour

	private void Awake()
	{
		instance = this;
	}

	// Default, initialize first scene to Tracker static
	void Start()
	{
		SceneManager.LoadScene(firstScene, LoadSceneMode.Additive);
	}

	#endregion MonoBehaviour

	#region Loading

	// Get loading state bool
	public bool GetLoadingState()
	{
		return isLoading;
	}

	// Set loading state bool
	public void SetLoadingState(bool b)
	{
		isLoading = b;
	}

	public void Reload()
	{
		// Load correct scene depending of TrackingMode
		switch (TrackerDetection.instance.GetTrackingMode())
		{
			case TrackingMode.Static:
			{
				StartCoroutine(LoadAdditive("1_Tracker_fixe", TrackingMode.Static));
				break;
			}

			case TrackingMode.MultipleScene:
			{
				StartCoroutine(LoadAdditive("2_Multiple_trackers_scene01", TrackingMode.MultipleScene));
				break;
			}

			case TrackingMode.MultiplePosition:
			{
				StartCoroutine(LoadAdditive("3_Multiple_trackers_position", TrackingMode.MultiplePosition));
				break;
			}

			case TrackingMode.Dynamic:
			{
				StartCoroutine(LoadAdditive("4_Tracker_mobile", TrackingMode.Dynamic));
				break;
			}


			case TrackingMode.NoTracker:
			{
				StartCoroutine(LoadAdditive("5_No_Tracker", TrackingMode.NoTracker));
				break;
			}
		}
	}

	public IEnumerator LoadAdditive(string name, TrackingMode TM = TrackingMode.Null)
	{
		SetLoadingState(true);

		// Fade In
		CanvasManager.instance.Fade(true);
		yield return new WaitForSecondsRealtime(CanvasManager.instance.GetFadeTime());

		// Destroy AR Session Origin
		CanvasManager.instance.DebugLine("Destroy old ARSessionOrigin");

		Destroy(GameObject.FindObjectOfType<ARSessionOrigin>().gameObject);

		//yield return new WaitForEndOfFrame();
		yield return new WaitForSecondsRealtime(0.1f);

		// Instantiate new AR Session Origin
		GameObject ARsession = Instantiate(ARSessionOrigin, null);
		ARsession.name = "ARSessionOrigin";

		CanvasManager.instance.DebugLine("Instantiate a new ARSessionOrigin");

		yield return new WaitForSecondsRealtime(0.1f);

		// Reset Dependencies to relink ARTrackedImageManager
		TrackerDetection.instance.ResetDependencies();
		yield return new WaitForSecondsRealtime(0.1f);

		// Disable TrackerDetection to call OnDisable
		TrackerDetection.instance.gameObject.SetActive(false);
		yield return new WaitForSecondsRealtime(0.1f);

		// Enable TrackerDetection to call OnEnable
		TrackerDetection.instance.gameObject.SetActive(true);
		yield return new WaitForEndOfFrame();

		// Set correct TrackingMode
		if (TM != TrackingMode.Null)
		{
			TrackerDetection.instance.SetTrackingMode(TM);
		}
		yield return new WaitForEndOfFrame();

		TrackerDetection.instance.ResetAll();

		// Unload scenes related to Tracking
		int amount = SceneManager.sceneCount;
		for (int i = 0; i < amount; i++)
		{
			if (trackingScenes.Contains<string>(SceneManager.GetSceneAt(i).name))
			{
				AsyncOperation unload = SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
				while (!unload.isDone)
				{
					yield return new WaitForEndOfFrame();
				}
			}
		}

		yield return new WaitForEndOfFrame();

		// Load correct scene
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

		while (!asyncLoad.isDone)
		{
			yield return new WaitForEndOfFrame();
		}

		// Fade Out
		CanvasManager.instance.Fade(false);
		SetLoadingState(false);

	}

	// Load additive on single scene
	public void LoadSingle(int val)
	{
		switch (val) // 2 Scenes for Multiple Tracker Scenes, if equal to two, load scene multiple trackers 01
		{
			case 0:
			{
				StartCoroutine(LoadAdditive("1_Tracker_fixe", TrackingMode.Static));
				break;
			}
			case 1:
			{
				StartCoroutine(LoadAdditive("2_Multiple_trackers_scene01", TrackingMode.MultipleScene));
				break;
			}
			case 2:
			{
				StartCoroutine(LoadAdditive("3_Multiple_trackers_position", TrackingMode.MultiplePosition));
				break;
			}
			case 3:
			{
				StartCoroutine(LoadAdditive("4_Tracker_mobile", TrackingMode.Dynamic));
				break;
			}
			case 4:
			{
				StartCoroutine(LoadAdditive("5_No_Tracker", TrackingMode.NoTracker));
				break;
			}
		}
	}

	// Extern call for asyncLoad (multiple scenes)
	public void ExternalAsyncLoad(string sceneName)
	{
		StartCoroutine(AsyncLoad(sceneName));
	}

	// Async load coroutine, similar to LoadAdditive
	public IEnumerator AsyncLoad(string sceneName)
	{
		SetLoadingState(true);

		// Fade In
		CanvasManager.instance.Fade(true);
		yield return new WaitForSecondsRealtime(CanvasManager.instance.GetFadeTime());

		// Destroy old ARSessionOrigin
		Destroy(GameObject.FindObjectOfType<ARSessionOrigin>().gameObject);
		CanvasManager.instance.DebugLine("Destroy old session");

		yield return new WaitForSecondsRealtime(0.1f);

		// Instantiate new ARSessionOrigin
		GameObject ARsession = Instantiate(ARSessionOrigin, null);
		ARsession.name = "ARSessionOrigin";
		CanvasManager.instance.DebugLine("Instantiate new session");

		yield return new WaitForSecondsRealtime(0.1f);

		// Reset Dependencies for ARTrackedImageManager
		TrackerDetection.instance.ResetDependencies();
		CanvasManager.instance.DebugLine("Reset Dependencies");

		yield return new WaitForSecondsRealtime(0.1f);

		// Disable TrackerDetection to trigger OnDisable
		TrackerDetection.instance.gameObject.SetActive(false);
		CanvasManager.instance.DebugLine("Disable TD");

		yield return new WaitForSecondsRealtime(0.1f);

		// Enable TrackerDetection to trigger OnEnable
		TrackerDetection.instance.gameObject.SetActive(true);
		CanvasManager.instance.DebugLine("Enable TD");

		yield return new WaitForEndOfFrame();
		TrackerDetection.instance.ResetAll();
		TrackerDetection.instance.ResetTrackingInfo();
		yield return new WaitForEndOfFrame();

		TrackerDetection.instance.SetTrackingMode(TrackingMode.MultipleScene);
		yield return new WaitForEndOfFrame();

		//yield return new WaitForEndOfFrame();

		CanvasManager.instance.DebugLine("Async Unload ");

		// Unload scenes related to Tracking
		int amount = SceneManager.sceneCount;
		for (int i = 0; i < amount; i++)
		{
			if (trackingScenes.Contains<string>(SceneManager.GetSceneAt(i).name))
			{
				AsyncOperation unload = SceneManager.UnloadSceneAsync(SceneManager.GetSceneAt(i));
				while (!unload.isDone)
				{
					yield return new WaitForEndOfFrame();
				}
			}
		}

		yield return new WaitForEndOfFrame();
		CanvasManager.instance.DebugLine("Async Load");

		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		while (!asyncLoad.isDone)
		{
			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForEndOfFrame();

		CanvasManager.instance.Fade(false);
		CanvasManager.instance.DebugLine("Scene loaded, remove previous one");
		SetLoadingState(false);
	}

	// Check if scene with name s is currently loaded
	public bool SceneAlreadyLoaded(string s)
	{
		for(int i =0; i<SceneManager.sceneCount;i++)
		{
			if(SceneManager.GetSceneAt(i).name.Equals(s))
			{
				return true;
			}
		}
		return false;
	}

	// Get first non system scene
	public string GetFirstNonSystemScene()
	{
		for(int i = 0; i<SceneManager.sceneCount;i++)
		{
			if(SceneManager.GetSceneAt(i).name != "0_System")
			{
				return SceneManager.GetSceneAt(i).name;
			}
		}

		return "";
	}

	#endregion Loading
}

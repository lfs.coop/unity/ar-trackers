﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.SceneManagement;

public enum TrackingMode
{
	Null,
	Static,
	MultipleScene,
	MultiplePosition,
	Dynamic,
	NoTracker
}


public class TrackerDetection : MonoBehaviour
{
	#region Dependencies
	public static TrackerDetection instance;

	private ARTrackedImageManager ARTrackedImageManager = null;
	private ARSessionOrigin AROrigin_ref = null;
	private ARTrackedImage trackedImage = null;
	private Coroutine TrackingComputing = null;

	// Multiple scenes tracking list
	private Dictionary<string, string> Scenes;

	// Fixed List of images tracker names ang GameObject related (All modes except Multiple Scenes)
	private Dictionary<string, GameObject> Images;

	// Dynamic List of dynamics images tracker names and Transform related (Tracking Dynamic)
	private Dictionary<string, Transform> DynamicImages;

	// Dynamic List of image tracker names and Transform to related objects (Multiple Positions)
	private Dictionary<string, Transform> MultiplePositions;

	// Dynamic List used to store already tracked images
	private List<string> ignoredImages;

	[Tooltip("Current tracking mode from enum TrackingMode")]
	[SerializeField]
	protected TrackingMode currentTrackingMode = TrackingMode.Static;

	[Tooltip("Minimum frame count for tracking checker")]
	[SerializeField]
	private int minimalNumberOfFrames = 30;

	[Tooltip("Minimum distance for tracking checker")]
	[SerializeField]
	public float minimalDistance = 0.005f;

	[Tooltip("Security length before to track again")]
	[SerializeField]
	private float securityTimerLength = 3f;

	private float currentSecurity = 0f;

	[Tooltip("Minimum distance to trigger ajustment on position")]
	[SerializeField]
	private float minDistanceLerp = 0.5f;

	[Tooltip("Minimum distance for removing position tracker from ignored list")]
	[SerializeField]
	private float minDistanceExit = 0.75f;

	[Tooltip("Minimum time to lerp position for ajustment")]
	[SerializeField]
	private float minTimeLerp = 1.0f;

	[Tooltip("Maximum time to lerp position for ajustment")]
	[SerializeField]
	private float maxTimeLerp = 5.0f;

	[Tooltip("Number of tracker for multiple position")]
	[SerializeField]
	private int multiplePositionTrackerCount = 2;

	[Tooltip("Trackers names for changing scene at tracking")]
	public string[] TrackerNames;
	[Tooltip("Scenes names for changing scene at tracking")]
	public string[] ScenesNames;


	[Tooltip("Currently tracking ?")]
	[SerializeField]
	private bool TrackingON = false;

	[Tooltip("Currently smoothing position ?")]
	[SerializeField]
	private bool SmoothChange = false;

	#endregion Dependencies

	#region MonoBehaviour

	private void Awake()
	{
		Initialize();
	}

	private void Start()
	{
		FeedScenesReferences();
	}

	private void FixedUpdate()
	{
		// Check distance for position tracker, if over minDistanceExit, remove it from ignored

		if(currentTrackingMode.Equals(TrackingMode.MultiplePosition))
		{
			for (int i = ignoredImages.Count - 1; i >= 0; i--)
			{
				if (MultiplePositions.ContainsKey(ignoredImages[i]))
				{
					if (Vector3.Distance(Camera.main.transform.position, MultiplePositions[ignoredImages[i]].position) >= minDistanceExit)
					{
						ignoredImages.RemoveAt(i);
					}
				}
			}
		}

		CheckTrackedImage();
	}

	// Initializer for instance, lists and component references
	private void Initialize()
	{
		instance = this;
		ignoredImages = new List<string>();
		Scenes = new Dictionary<string, string>();
		Images = new Dictionary<string, GameObject>();
		DynamicImages = new Dictionary<string, Transform>();
		MultiplePositions = new Dictionary<string, Transform>();
		ARTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
		AROrigin_ref = FindObjectOfType<ARSessionOrigin>();
	}

	// Scenes List construction
	private void FeedScenesReferences()
	{
		for (int i = 0; i < Mathf.Min(TrackerNames.Length, ScenesNames.Length); i++)
		{
			Scenes.Add(TrackerNames[i], ScenesNames[i]);
		}
	}

	#endregion MonoBehaviour

	#region ARTracking

	// Image change reference, needed for Dynamic Tracking
	private void OnEnable()
	{
		ARTrackedImageManager.trackedImagesChanged += ImageChanged;
	}

	// Image change reference, needed for Dynamic Tracking
	private void OnDisable()
	{
		ARTrackedImageManager.trackedImagesChanged -= ImageChanged;
	}

	// Image change behaviour, called on added, updated and removed trackers
	public void ImageChanged(ARTrackedImagesChangedEventArgs eventArgs)
	{
		foreach (ARTrackedImage trackedImage in eventArgs.added)
		{
			UpdateImage(trackedImage);
		}

		foreach (ARTrackedImage trackedImage in eventArgs.updated)
		{
			UpdateImage(trackedImage);
		}

		foreach (ARTrackedImage trackedImage in eventArgs.removed)
		{
			CanvasManager.instance.DebugLine("Tracked Image " + trackedImage.name + " was removed at " + Time.deltaTime);
		}
	}

	// Update transform for continuous tracking
	private void UpdateImage(ARTrackedImage trackedImage)
	{
		string name = trackedImage.referenceImage.name;

		if (DynamicImages.ContainsKey(name))
		{
			DynamicImages[name].position = trackedImage.transform.position;
			DynamicImages[name].rotation = trackedImage.transform.rotation;
		}



		if (!ignoredImages.Contains(name)&& MultiplePositions.ContainsKey(name) && MultiplePositions.Count > multiplePositionTrackerCount - 1 && currentSecurity <= 0f && !SmoothChange)
		{
			// If distance is lower than minDistanceLerp && Tracking state is ok
			if (Vector3.Distance(Camera.main.transform.position, trackedImage.transform.position) <= minDistanceLerp && trackedImage.trackingState.Equals(UnityEngine.XR.ARSubsystems.TrackingState.Tracking))
			{
				currentSecurity = securityTimerLength;
				CallSmoothChange(MultiplePositions[name].gameObject, trackedImage.transform);
			}
		}
	}

	// Main tracking routine, foreach tracking mode except NoTracker, the global behaviour is the same for all :
	// When a tracker is identified, this function is called. Instead of triggering associated behaviour, let's check
	// continuity on tracker position. A possible improvment will be to deploy this check to rotation over time.
	public IEnumerator Tracking(Vector3 initialPos)
	{
		// Loop condition
		bool stable = false;

		// Distance for analysis
		float distanceDifferential = 0;

		// Ok frames, distance under minimal threshold
		int stableFrames = 0;

		// Store initial position, and update this Vector3 over time
		Vector3 comparePosition = initialPos;

		// Name of tracked image
		string imageName = trackedImage.referenceImage.name;

		CanvasManager.instance.DebugLine("Tracked image founded " + imageName);

		// Depending of tracking mode
		switch (currentTrackingMode)
		{
			case TrackingMode.Static:
			{
				GameObject ObjectToAppear = null;
				CanvasManager.instance.DebugLine("Starting static tracking");

				// Getting associated object to static tracker
				if (Images.ContainsKey(imageName))
				{
					// Activating Panel Tracking
					CanvasManager.instance.SetTrackingPanel(true);
					ObjectToAppear = Images[imageName];
					CanvasManager.instance.DebugLine("Object was founded at");
				}
				yield return new WaitForEndOfFrame();

				while (!stable)
				{
					// If object exists
					if (ObjectToAppear != null)
					{
						// Update UI ratio for tracking indicator
						CanvasManager.instance.RefreshRatio((float)stableFrames / (float)minimalNumberOfFrames);

						// Store distance differential
						distanceDifferential = Vector3.Distance(comparePosition, trackedImage.transform.position);

						// Check compare to minimal distance
						if (distanceDifferential < minimalDistance)
						{
							stableFrames++; // Distance ok, keep going
						}
						else
						{
							stableFrames = 0; // Distance changed, reset count
						}

						// If number of Ok frames is correct, we can trigger associated behaviour
						if (stableFrames >= minimalNumberOfFrames && !stable)
						{
							stable = true;
							CanvasManager.instance.DebugLine("Stable now, make content appears");

							// Change ARSessionOrigin transform to make content appears at the correct place
							AROrigin_ref.MakeContentAppearAt(ObjectToAppear.transform, trackedImage.transform.position, trackedImage.transform.rotation);
							break;
						}

						// Update position for analysis
						comparePosition = trackedImage.transform.position;
						yield return new WaitForEndOfFrame();
					}
					else // Associated object not founded
					{
						CanvasManager.instance.DebugWarning("Object to move was not founded");
						break;
					}
				}

				if (trackedImage != null) // If image ok, store if in ignoredImages
				{
					yield return new WaitForEndOfFrame();
					CanvasManager.instance.DebugLine("Ignore tracked image");
					ignoredImages.Add(imageName);
				}
				break;
			}

			case TrackingMode.MultipleScene:
			{
				CanvasManager.instance.DebugLine("Multiple scene tracking");

				// Check that image name corresponds to an item on Scenes AND scene to load is different from current && not loading
				if (Scenes.ContainsKey(imageName) && !SceneLoader.instance.SceneAlreadyLoaded(Scenes[imageName]))
				{
					CanvasManager.instance.SetTrackingPanel(true);

					// Waiting one frame to compare positions
					yield return new WaitForEndOfFrame();

					while (!stable)
					{
						// Update UI ratio for tracking indicator
						CanvasManager.instance.RefreshRatio((float)stableFrames / (float)minimalNumberOfFrames);

						// Store distance differential
						distanceDifferential = Vector3.Distance(comparePosition, trackedImage.transform.position);

						// Check compare to minimal distance
						if (distanceDifferential < minimalDistance)
						{
							stableFrames++;  // Distance ok, keep going
						}
						else
						{
							stableFrames = 0; // Distance changed, reset count
						}
						if (stableFrames >= minimalNumberOfFrames)
						{
							stable = true;
							
							// Stable, calling Async Load on corresponding scene
							CanvasManager.instance.DebugLine("ASYNC LOAD called");
							SceneLoader.instance.ExternalAsyncLoad(Scenes[imageName]);
							break;
						}

						// Update position for analysis
						comparePosition = trackedImage.transform.position;
						yield return new WaitForEndOfFrame();
					}
				}
				else
				{
					CanvasManager.instance.DebugWarning("No key with the name " + imageName + " was found in Dictionnary or already loaded");
				}
				break;
			}

			case TrackingMode.MultiplePosition:
			{
				GameObject ObjectToAppear = null;
				CanvasManager.instance.DebugLine("Starting static tracking");
				if (Images.ContainsKey(imageName))
				{
					CanvasManager.instance.SetTrackingPanel(true);

					ObjectToAppear = Images[imageName];
					CanvasManager.instance.DebugLine("Object was founded");
				}
				yield return new WaitForEndOfFrame();
				
				while (!stable)
				{
					// If object exists
					if (ObjectToAppear != null)
					{
						// Update UI ratio for tracking indicator
						CanvasManager.instance.RefreshRatio((float)stableFrames / (float)minimalNumberOfFrames);

						// Store distance differential
						distanceDifferential = Vector3.Distance(comparePosition, trackedImage.transform.position);
						if (distanceDifferential < minimalDistance)
						{
							stableFrames++;  // Distance ok, keep going
						}
						else
						{
							stableFrames = 0; // Distance changed, reset count
						}

						if (stableFrames >= minimalNumberOfFrames && !stable)
						{
							stable = true;

							// If an image was already tracked -> Change position smoothly
							if (MultiplePositions.Count > 0)
							{
								CallSmoothChange(ObjectToAppear, trackedImage.transform);
							}
							else // else, first tracked image -> Make content appear without smooth
							{
								AROrigin_ref.MakeContentAppearAt(ObjectToAppear.transform, trackedImage.transform.position, trackedImage.transform.rotation);
							}

							// Add it to ignore list and MultiplePosition list for auto update
							ignoredImages.Add(imageName);
							MultiplePositions.Add(imageName, ObjectToAppear.transform);
							break;
						}

						// Update position for analysis
						comparePosition = trackedImage.transform.position;
						yield return new WaitForEndOfFrame();
					}
					else
					{
						CanvasManager.instance.DebugWarning("Object to move was not founded");
						break;
					}
				}

				break;
			}

			case TrackingMode.Dynamic:
			{
				CanvasManager.instance.DebugLine("Starting dynamic tracking");
				// Waiting one frame to compare positions
				bool imageOk = false;

				// If object exists
				if (Images.ContainsKey(imageName))
				{
					imageOk = true;
					CanvasManager.instance.SetTrackingPanel(true);
					CanvasManager.instance.DebugLine("Object was founded");
				}
				yield return new WaitForEndOfFrame();

				while (!stable)
				{
					if (imageOk)
					{
						// Update UI ratio for tracking indicator
						CanvasManager.instance.RefreshRatio((float)stableFrames / (float)minimalNumberOfFrames);

						// Store distance differential
						distanceDifferential = Vector3.Distance(comparePosition, trackedImage.transform.position);
						if (distanceDifferential < minimalDistance)
						{
							stableFrames++; // Distance ok, keep going
						}
						else
						{
							stableFrames = 0; // Distance changed, reset count
						}

						if (stableFrames >= minimalNumberOfFrames)
						{
							stable = true;

							CanvasManager.instance.DebugLine("Make content appears called");

							// Instantiate object 
							GameObject Go = Instantiate(Images[imageName], null);
							Go.transform.position = trackedImage.transform.position;
							Go.transform.rotation = trackedImage.transform.rotation;

							// Move object outside of System scene -> Getting first non system scene
							SceneManager.MoveGameObjectToScene(Go, SceneManager.GetSceneByName(SceneLoader.instance.GetFirstNonSystemScene()));
							yield return new WaitForEndOfFrame();

							// Add reference to Dynamic list for update behaviour
							DynamicImages.Add(imageName, Go.transform);

							// Add reference to ignored images
							ignoredImages.Add(imageName);

							break;
						}
						// Update position for analysis
						comparePosition = trackedImage.transform.position;
						yield return new WaitForEndOfFrame();
					}
					else
					{
						CanvasManager.instance.DebugLine("Object to move was not founded");
						break;
					}
					yield return new WaitForEndOfFrame();
				}
				break;
			}
		}
		CanvasManager.instance.SetTrackingPanel(false);
		StartCoroutine(Security());
		yield return null;
	}

	// Smooth position change over distance and time
	public IEnumerator SmoothOriginChange(GameObject Go, Transform image)
	{
		// Lerp based on distance, let's compute it
		float distance = Vector3.Distance(Go.transform.position, image.position);
		float timer = 0f;

		// Example, for a distance of 25cm between positions, we are going to wait 7.5 seconds and clamped to min/max
		float maxTime = Mathf.Clamp(distance * 30f, minTimeLerp, maxTimeLerp);

		CanvasManager.instance.DebugLine("Starting smoothed change on " + image.name + " with dis = " + distance + " - and time " + maxTime);

		Vector3 initialPos = Go.transform.position;
		Quaternion initialRot = Go.transform.rotation;

		// Over duration, call MakeContentAppearAt on the position Lerp
		while (timer < maxTime)
		{
			AROrigin_ref.MakeContentAppearAt(Go.transform, Vector3.Lerp(initialPos, image.position, timer / maxTime), Quaternion.Lerp(initialRot, image.rotation, timer / maxTime));
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		CanvasManager.instance.DebugLine("Ending smoothed change on " + image.name + " with dis = " + distance + " - and time " + maxTime);

		// Final position call, appear at final position
		AROrigin_ref.MakeContentAppearAt(Go.transform, image.position, image.rotation);

		// Launch Security routine
		currentSecurity = securityTimerLength;
		SmoothChange = false;

		string name = image.gameObject.GetComponent<ARTrackedImage>().referenceImage.name;

		if (!ignoredImages.Contains(name))
		{
			ignoredImages.Add(name);
		}

		StartCoroutine(Security());
	}

	// Search for Tracked Image
	private void CheckTrackedImage()
	{
		// Check if tracked image detected
		ARTrackedImage image = null;
		image = FindObjectOfType<ARTrackedImage>();

		// if image exists, traking is currently off and tracking state is ok
		if (image != null && !TrackingON && image.trackingState.Equals(UnityEngine.XR.ARSubsystems.TrackingState.Tracking))
		{
			// Check that image is not ignored, security timer is under or equal 0 and there is no scene actually loading
			if (!ignoredImages.Contains(image.referenceImage.name) && currentSecurity <= 0f && !SceneLoader.instance.GetLoadingState())
			{
				trackedImage = image;
				TrackingON = true;
				TrackingComputing = StartCoroutine(Tracking(trackedImage.transform.position));
				currentSecurity = securityTimerLength;
			}
		}
	}

	// Call Smooth position changed coroutine
	public void CallSmoothChange(GameObject Go, Transform image)
	{
		SmoothChange = true;
		StartCoroutine(SmoothOriginChange(Go, image));
	}

	// Security coroutine during tracking
	public IEnumerator Security()
	{
		yield return new WaitForSecondsRealtime(currentSecurity);
		currentSecurity = 0f;
		TrackingON = false;

	}

	// Extern call for objects placed in scene, add reference to image if not already existing
	public void SetObject(string id, GameObject Go)
	{
		if (!Images.ContainsKey(id) || (Images.ContainsKey(id) && !Images.ContainsValue(Go)))
		{
			Images.Add(id, Go);
		}
	}

	// Get current TrackingMode
	public TrackingMode GetTrackingMode()
	{
		return currentTrackingMode;
	}

	// Set current TrackingMode with parameter value
	public void SetTrackingMode(TrackingMode TM)
	{
		currentTrackingMode = TM;
	}

	// Get scene name corresponding to tracker, return "" if reference is missing
	public string GetSceneAt(string trackerName)
	{
		if (Scenes != null && Scenes.Count > 0)
		{
			if (Scenes.ContainsKey(trackerName))
			{
				return Scenes[trackerName];
			}
			else
			{
				CanvasManager.instance.DebugWarning("No founded associated tracker with name " + trackerName + " in Scenes list");
				return "";
			}
		}
		else
		{
			CanvasManager.instance.DebugWarning("No founded associated tracker with name " + trackerName + " in Scenes list");
			return "";

		}
	}

	#endregion ARTracking 

	#region Clear Methods

	// Clear images list
	private void ClearObjects()
	{
		Images.Clear();
	}

	// Clear Dynamic images list
	private void ClearDynamic()
	{
		DynamicImages.Clear();
	}

	// Clear Multiple positions list
	private void ClearMultiplePositions()
	{
		MultiplePositions.Clear();
	}

	// Relink ARTrackedImageManager and ARSessionOrigin, used at reload or load new scene
	public void ResetDependencies()
	{
		CanvasManager.instance.DebugLine("Reset Dependencies");

		ARTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
		AROrigin_ref = FindObjectOfType<ARSessionOrigin>();
	}

	// Clear ignored images list
	public void ResetTrackingImageManage()
	{
		ignoredImages.Clear();
		ignoredImages = new List<string>();
	}

	// Clear used disctionnaries
	public void ResetDictionnaries()
	{
		ClearDynamic();
		ClearObjects();
		ClearMultiplePositions();
	}

	// Global reset, clearing dictionnaies and ignored images list
	public void ResetAll()
	{
		CanvasManager.instance.DebugLine("Reset dictionnaries Reset TrackingImageManage");
		ResetDictionnaries();
		ResetTrackingImageManage();
	}

	// Manually reset security and stop all coroutines
	public void ResetTrackingInfo()
	{
		StopAllCoroutines();
		TrackingON = false;
		currentSecurity = 0f;
	}

	// Manually add scenes references for Multiple Scenes
	public void RefreshScenes()
	{
		if (currentTrackingMode.CompareTo(TrackingMode.MultipleScene) == 0)
		{
			for (int i = 0; i < Mathf.Min(TrackerNames.Length, ScenesNames.Length); i++)
			{
				if (!Scenes.ContainsKey(TrackerNames[i]))
				{
					Scenes.Add(TrackerNames[i], ScenesNames[i]);
				}
			}
		}
	}

	#endregion Clear Methods
}

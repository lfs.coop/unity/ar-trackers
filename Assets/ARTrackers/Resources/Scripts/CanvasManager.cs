﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
	#region Dependencies
	public static CanvasManager instance;

	[Tooltip("Debug mode activated ?")]
	[SerializeField]
	private bool Debugging = false;

	[Tooltip("Tracking panel, used at Tracking")]
	[SerializeField]
	private GameObject TrackingPanel;

	[Tooltip("Tracking image, filled with ratio")]
	[SerializeField]
	private Image TrackingImage;

	[Tooltip("Black panel use to fade")]
	[SerializeField]
	private Image FadeBackground;

	[Tooltip("Custom debug panel reference")]
	[SerializeField]
	private GameObject DebugPanel;

	[Tooltip("Clear custom console content")]
	[SerializeField]
	private GameObject ClearButton;

	// Transform dependence for hand made debug console
	[Tooltip("Transform parent to spawn console debug lines")]
	[SerializeField]
	private Transform ContentRoot;

	// Line prefab used as console line
	[Tooltip("Line prefab")]
	[SerializeField]
	private GameObject Line;

	[Tooltip("Colors used for tracking color fade")]
	[SerializeField]
	private Color CStart, CEnd;

	[Tooltip("Colors used for Fade In / Fade Out")]
	[SerializeField]
	private Color fadeON, fadeOFF;

	[Tooltip("Fade duration")]
	[SerializeField]
	private float fadeTime = 1.5f;


	#endregion Dependencies

	#region MonoBehaviour

	private void Awake()
	{
		instance = this;
		DebugPanel.SetActive(Debugging); // Activate debugging panel if needed
	}

	// Fade Out at start
	private void Start()
	{
		DebugPanel.SetActive(Debugging);
		ClearButton.SetActive(Debugging);
		Fade(false);
	}

	#endregion MonoBehaviour

	// Used to bypass the lack of debug console, using a scroll view and spawning lines prefabs
	#region Debug

	// Spawning debug line into scroll view and print it in console
	public void DebugLine(string l)
	{
		if(Debugging)
		{
			GameObject L = Instantiate(Line, ContentRoot);
			L.GetComponent<Text>().text = l;
			L.GetComponent<Text>().color = Color.white;
			Debug.Log(l);
		}
	}

	// Spawning debug warning into scroll view and print it in console
	public void DebugWarning(string l)
	{
		if(Debugging)
		{
			GameObject L = Instantiate(Line, ContentRoot);
			L.GetComponent<Text>().text = l;
			L.GetComponent<Text>().color = Color.yellow;
			Debug.LogWarning(l);
		}
	}

	// Delete all spawned lines
	public void ClearConsole()
	{
		foreach (Transform child in ContentRoot)
		{
			Destroy(child.gameObject);
		}
	}

	#endregion Debug

	#region UI

	// Enable/Disable tracking message panel 
	public void SetTrackingPanel(bool active)
	{
		TrackingPanel.SetActive(active);
	}

	// Set the ratio to the fillAmount for filled image and lerp the color
	public void RefreshRatio(float ratio)
	{
		TrackingImage.fillAmount = ratio;
		TrackingImage.color = Color.Lerp(CStart, CEnd, ratio);
	}

	// Fade function, calling FadeRoutine with bool parameter (true -> FadeIn)
	public void Fade(bool IN)
	{
		if(FadeBackground!=null)
		{
			StartCoroutine(FadeRoutine(IN));
		}
	}

	// Fade coroutine used to FadeIn and FadeOut on background image color
	public IEnumerator FadeRoutine(bool IN)
	{
		float timer = 0f;

		while (timer < fadeTime)
		{
			if (IN)
			{
				SetBackgroundColor(Color.Lerp(fadeOFF, fadeON, timer / fadeTime));
			}
			else
			{
				SetBackgroundColor(Color.Lerp(fadeON, fadeOFF, timer / fadeTime));
			}
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		if (IN)
		{
			SetBackgroundColor(fadeON);
		}
		else
		{
			SetBackgroundColor(fadeOFF);
		}
	}

	// Set Background color to C
	public void SetBackgroundColor(Color C)
	{
		if(FadeBackground!=null)
		{
			FadeBackground.color = C;
		}
	}

	// Get the fade duration
	public float GetFadeTime()
	{
		return fadeTime;
	}

	#endregion UI

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tracker : MonoBehaviour
{
	#region Dependencies

	// TrackingMode enum to see clearly if tracker is Static or Dynamic
	public enum TrackingMode
	{
		Static,
		Dynamic
	}

	[Tooltip("Tracking mode for this Tracker")]
	[SerializeField]
	protected TrackingMode tracking;

	[Tooltip("Image name corresponding to image to track")]
	[SerializeField]
	protected string imageName = "";

	[Tooltip("Is this tracker is used to correct position ?")]
	[SerializeField]
	protected bool correctingPosition = false;

	[Tooltip("Is this tracker is used to load scene ?")]
	[SerializeField]
	protected bool sceneLoading = false;

	[Tooltip("LoadSceneMode used for loading scene")]
#if UNITY_EDITOR
	[ConditionalHide("sceneLoading", false)] 
#endif
	[SerializeField]
	protected LoadSceneMode loadingMode = LoadSceneMode.Additive;

	[Tooltip("Corresponding scenes to load")]
	[SerializeField]
#if UNITY_EDITOR
	[ConditionalHide("sceneLoading", false)]
#endif
	protected string[] scenesToLoad;

	[Tooltip("Corresponding scenes to unload")]
	[SerializeField]
#if UNITY_EDITOR
	[ConditionalHide("sceneLoading", false)]
#endif
	protected string[] scenesToUnload;

	[Tooltip("Does the loading trigger a fade in / fade out")]
	[SerializeField]
	protected bool fading = false;

	[Tooltip("Is the object to move is instanciated at realtime ?")]
	[SerializeField]
	protected bool instanciated = false;

	[Tooltip("Reference to object to move, can be null")]
	[SerializeField]
	protected GameObject objectToMove = null;

	[Tooltip("Is the tracker will time out after X seconds. If equals to 0, time out disabled")]
	[SerializeField]
	protected float timeOut = 0;

	[Tooltip("Is the time out is forever ?")]
	[SerializeField]
	protected bool timedOutForever = false;

	[Tooltip("Is the time out destroy the related object ?")]
	[SerializeField]
	protected bool destroyObjectAfterTimeout = false;

	[Tooltip("Is the render stop when object is lost ?")]
	[SerializeField]
	protected bool stopRenderWhenLost = false;

	[SerializeField]
	protected Vector3 positionOffset = Vector3.zero;

	[SerializeField]
	protected Quaternion rotationOffset = Quaternion.identity;


	[SerializeField]
	protected bool UseOffset = false;

	// Internal parameter, only used to store rendering status and avoid checking from TrackingManager
	protected bool rendering = true;


	#endregion Dependencies

	#region Getters & Setters

	public void SetUseOffset(bool b)
	{
		UseOffset = b;
	}

	public bool GetUseOffset()
	{
		return UseOffset;
	}

	public TrackingMode GetTrackingMode()
	{
		return tracking;
	}

	public Vector3 GetPositionOffset()
	{
		return positionOffset;
	}

	public Quaternion GetRotationOffset()
	{
		return rotationOffset;
	}

	public void SetPositionOffset(Vector3 v)
	{
		positionOffset = v;
	}

	public void SetRotationOffset(Quaternion q)
	{
		rotationOffset = q;
	}

	public string GetImageName()
	{
		return imageName;
	}

	public bool GetCorrectingPosition()
	{
		return correctingPosition;
	}

	public bool GetSceneLoading()
	{
		return sceneLoading;
	}

	public LoadSceneMode GetLoadingMode()
	{
		return loadingMode;
	}

	public string[] GetScenesToLoad()
	{
		return scenesToLoad;
	}

	public string[] GetSceneToUnload()
	{
		return scenesToUnload;
	}

	public bool GetFading()
	{
		return fading;
	}

	public bool GetInstantiated()
	{
		return instanciated;
	}

	public GameObject GetObjectToMove()
	{
		return objectToMove;
	}

	public float GetTimeOut()
	{
		return timeOut;
	}

	public bool GetTimeOutStatus()
	{
		return timedOutForever;
	}

	public bool GetDestroyStatus()
	{
		return timedOutForever;
	}

	public bool GetStopRenderStatus()
	{
		return stopRenderWhenLost;
	}

	public bool GetRendering()
	{
		return rendering;
	}

	public void SetRendering(bool render)
	{
		rendering = render;
	}

	#endregion Getters & Setters

	#region Constructors

	// Parametered Constructor
	public Tracker(TrackingMode TM, string name, bool correctingPos, bool sceneLoad, LoadSceneMode loadMode, string[] sToLoad, string[] sToUnload, bool f ,bool inst, GameObject obj, float time, bool timedOutF, bool destroy, bool stopRender, Vector3 offsetP, Quaternion offsetR)
	{
		tracking = TM;
		imageName = name;
		correctingPosition = correctingPos;
		sceneLoading = sceneLoad;
		loadingMode = loadMode;
		scenesToLoad = sToLoad;
		scenesToUnload = sToUnload;
		fading = f;
		instanciated = inst;
		objectToMove = obj;
		timeOut = time;
		timedOutForever = timedOutF;
		destroyObjectAfterTimeout = destroy;
		stopRenderWhenLost = stopRender;

		positionOffset = offsetP;
		rotationOffset = offsetR;

		// Custom security on parameters

		// Is this Static ?
		if (tracking.Equals(TrackingMode.Static))
		{
			instanciated = false;
			timeOut = 0;
			timedOutForever = false;
			destroyObjectAfterTimeout = false;
			stopRenderWhenLost = false;
		}
		else // Then is Dynamic
		{
			correctingPosition = false;
			if (timeOut <= 0f)
			{
				timedOutForever = false;
				destroyObjectAfterTimeout = false;
			}
		}

		if (!sceneLoading)
		{
			scenesToLoad = new string[0];
			scenesToUnload = new string[0];
		}
		else
		{
			timeOut = 0;
			timedOutForever = false;
			destroyObjectAfterTimeout = false;
			stopRenderWhenLost = false;

			if (loadingMode.Equals(LoadSceneMode.Single))
			{
				scenesToUnload = new string[0];
			}
		}
	}

	// Copy Constructor
	public Tracker(Tracker T)
	{
		tracking = T.GetTrackingMode();
		imageName = T.GetImageName();
		correctingPosition = T.GetCorrectingPosition();
		sceneLoading = T.GetSceneLoading();
		loadingMode = T.GetLoadingMode();
		scenesToLoad = T.GetScenesToLoad();
		scenesToUnload = T.GetSceneToUnload();
		fading = T.GetFading();
		instanciated = T.GetInstantiated();
		objectToMove = T.GetObjectToMove();
		timeOut = T.GetTimeOut();
		timedOutForever = T.GetTimeOutStatus();
		destroyObjectAfterTimeout = T.GetDestroyStatus();
		stopRenderWhenLost = T.GetStopRenderStatus();

		// Custom security on parameters

		// Is this Static ?
		if (tracking.Equals(TrackingMode.Static))
		{
			instanciated = false;
			timeOut = 0;
			timedOutForever = false;
			destroyObjectAfterTimeout = false;
			stopRenderWhenLost = false;
		}
		else // Then is Dynamic
		{
			correctingPosition = false;
			if (timeOut <= 0f)
			{
				timedOutForever = false;
				destroyObjectAfterTimeout = false;
			}
		}

		if (!sceneLoading)
		{
			scenesToLoad = new string[0];
			scenesToUnload = new string[0];
		}
		else
		{
			timeOut = 0;
			timedOutForever = false;
			destroyObjectAfterTimeout = false;
			stopRenderWhenLost = false;

			if (loadingMode.Equals(LoadSceneMode.Single))
			{
				scenesToUnload = new string[0];
			}
		}
	}

	#endregion Constructors

	#region Behaviour

	public void ChangeRendering(bool active)
	{
		CanvasManager.instance.DebugLine("Set rendering on " + GetImageName() + " to " + active);
		SetRenderRecursive(active, GetObjectToMove().transform);
	}

	// Recursive function setting all founded Mesh renderers component to "active" (enabled/disabled)
	public void SetRenderRecursive(bool active, Transform target)
	{
		MeshRenderer MR;
		if(target.gameObject.TryGetComponent<MeshRenderer>(out MR))
		{
			MR.enabled = active;
		}
		if(target.childCount>0)
		{
			for(int i =0;i<target.childCount;i++)
			{
				SetRenderRecursive(active, target.GetChild(i));
			}
		}
	}

	// Function called for related object movement
	public void UpdateObject(Transform target)
	{
		Transform T = GetObjectToMove().transform;
		T.position = target.position;
		T.rotation = target.rotation;
	}

	// On first frame, add this tracker instance to the TrackingManager
	private void Start()
	{
		TrackingManager.instance.AddTrackedObject(this);
	}

	// If object is spawned, update reference to instantiated scene object
	public void SetInstanceForObject(GameObject G)
	{
		objectToMove = G;
	}

	#endregion Behaviour
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ImageChangedBehaviour : MonoBehaviour
{

	private ARTrackedImageManager ARTrackedImageManager;
	private TrackerDetection TrackerDetect;
    // Start is called before the first frame update
    void Awake()
    {
		ARTrackedImageManager = GetComponent<ARTrackedImageManager>();
		TrackerDetect = TrackerDetection.instance;
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	
	private void OnEnable()
	{
		ARTrackedImageManager.trackedImagesChanged += TrackerDetect.ImageChanged;
		CanvasManager.instance.DebugLine("On Enable called -> += ImageChanged");
	}

	private void OnDisable()
	{
		ARTrackedImageManager.trackedImagesChanged -= TrackerDetect.ImageChanged;
		CanvasManager.instance.DebugLine("On Disable called -> += ImageChanged");

	}
}

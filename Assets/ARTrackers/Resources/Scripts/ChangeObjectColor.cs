﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeObjectColor : MonoBehaviour
{
	#region Dependencies

	private Transform targetCamera = null;
	// Material to change
	private Material mat;

	[Tooltip("Colors")]
	[SerializeField]
	private Color C1, C2, C3, C4;

	[Tooltip("Current Angle")]
	[SerializeField]
	private float angle = 0f;

	[Tooltip("Current mode, mode from 1 to 4, 4 different behaviours")]
	[SerializeField]
	private int mode = 0;

	[Tooltip("Currently change color ? ")]
	[SerializeField]
	private bool ChangeOn = false;

	// Used to store 2D position of camera, height is fixed to object one
	private Vector3 camPosition2D = Vector3.zero;

	#endregion Dependencies

	#region MonoBehaviour
	
	// Initialize material and get Camera.main transform
	// ! Warning ! -> AR Camera must be tagged as Main Camera
	void Start()
	{
		mat = GetComponent<MeshRenderer>().material;
		targetCamera = Camera.main.transform;
	}

	// Update is called once per frame
	void Update()
	{
		// If target ok
		if (targetCamera != null)
		{
			// Project camera to object height
			camPosition2D = new Vector3(targetCamera.position.x, transform.position.y, targetCamera.position.z);

			// Compute signed angle and clamp it between 0°-360°
			angle = Vector3.SignedAngle(transform.forward, transform.position - camPosition2D, Vector3.up);
			if (angle < 0f)
			{
				angle += 360f;
			}
		}

		// Analysis on angle and select behaviour

		if ((AngleOk(angle,315f,380f) || AngleOk(angle, 0f, 45f)) && mode != 0 && !ChangeOn) // C1
		{
			ChangeOn = true;
			LerpColorOverAngle(mat.GetColor("_BaseColor"), C1, 1f);
			mode = 0;
		}
		else if (AngleOk(angle, 45f, 135f) && mode != 1 && !ChangeOn) // C2
		{
			ChangeOn = true;
			LerpColorOverAngle(mat.GetColor("_BaseColor"), C2, 1f);
			mode = 1;
		}

		else if (AngleOk(angle, 135f, 225f) && mode != 2 && !ChangeOn) // C3
		{
			ChangeOn = true;
			LerpColorOverAngle(mat.GetColor("_BaseColor"), C3, 1f);
			mode = 2;
		}
		else if (AngleOk(angle, 225f, 315f) && mode != 3 && !ChangeOn) // C4
		{
			ChangeOn = true;
			LerpColorOverAngle(mat.GetColor("_BaseColor"), C4, 1f);
			mode = 3;
		}

	}
	#endregion MonoBehaviour

	#region Methods

	// Return true if angle is between minValue and maxValue (include minValue, exclude maxValue) 
	public bool AngleOk(float angle, float minValue, float maxValue)
	{
		return (angle >= minValue && angle < maxValue);
	}

	// Call change color routine
	public void LerpColorOverAngle(Color Cstart, Color Cend, float time)
	{
		StartCoroutine(LerpColor(Cstart, Cend, time));
	}

	// Change color over time
	public IEnumerator LerpColor(Color Cstart, Color Cend, float length)
	{
		float timer = 0f;

		while (timer < length)
		{
			ChangeColor(Color.Lerp(Cstart, Cend, timer / length));
			timer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForEndOfFrame();

		ChangeColor(Cend);
		ChangeOn = false;

		yield return null;
	}

	// Update material colors to C
	public void ChangeColor(Color C)
	{
		mat.SetColor("_BaseColor", C);
		mat.SetColor("_EmissionColor", C);
	}

	#endregion Methods

}

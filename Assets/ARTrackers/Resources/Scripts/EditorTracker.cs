﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class EditorTracker : MonoBehaviour
{
	[SerializeField]
	protected Transform Target;

	[SerializeField]
	protected Vector3 relativePosition;

	[SerializeField]
	public Quaternion relativeRotation;

	public Tracker T;

	[SerializeField]
	protected float debugSphereRadius = 0.05f;

	public bool SetOffsets = false;

	private void Update()
	{
		if(Target!=null)
		{
			relativePosition = Target.position - transform.position;
			relativeRotation = Target.rotation * Quaternion.Inverse(transform.rotation);
		}

		if(SetOffsets)
		{
			T.SetPositionOffset(relativePosition);
			T.SetRotationOffset(relativeRotation);
			SetOffsets = false;
		}


	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(transform.position, debugSphereRadius);
		if(Target!=null)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, Target.position);
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(Target.position, debugSphereRadius);
		}

	}
}

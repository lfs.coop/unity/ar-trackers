﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
	private Dropdown D;

	private void Awake()
	{
		D = GetComponent<Dropdown>();
	}

	// If you need to extend the amount of scenes, be careful to respect the index order
	public void UpdateScene()
	{
		SceneManager.LoadScene(D.value);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ProjectWithoutTracker : MonoBehaviour
{
	#region Dependencies

	[Tooltip("Transform to spawn object, set to Origin position")]
	[SerializeField]
	private Transform SpawningZone;

	[Tooltip("Prefab to instantiate and spawn")]
	[SerializeField]
	public GameObject ObjectToSpawn;

	private ARSessionOrigin sessionOrigin = null;
	private ARPlaneManager planeManager = null;
	private ARRaycastManager raycastManager = null;

	[Tooltip("If set to true, spawn object once")]
	[SerializeField]
	private bool SpawnOnce = true;

	[Tooltip("Object already spawned ?")]
	[SerializeField]
	private bool spawned = false;

	[Tooltip("Limit object spawning only to ground")]
	[SerializeField]
	private bool onlyGround = false;

	// Current GameObject reference spawned
	private GameObject currentSpawned = null;

	// Minimum height stored for comparison
	private float minHeight = 50f;

	#endregion Dependencies

	#region MonoBehaviour

	void Start()
	{
		// Set References
		sessionOrigin = FindObjectOfType<ARSessionOrigin>();
		planeManager = sessionOrigin.GetComponent<ARPlaneManager>();
		raycastManager = sessionOrigin.GetComponent<ARRaycastManager>();

		// Activate components, default ARSessionOrigin get them disabled
		planeManager.enabled = true;
		raycastManager.enabled = true;

		// Instantiate prefab to move and spawn at touch
		currentSpawned = Instantiate(ObjectToSpawn, SpawningZone, true);
		currentSpawned.SetActive(false);
	}

	void Update()
	{
		// if we need to spawn again
		if (!SpawnOnce || (SpawnOnce && !spawned))
		{
			// Permanent raycast call to place object correctly
			if (raycastManager != null && planeManager != null && currentSpawned != null)
			{
				Raycast();
			}

			// Check touch imput and Spawn
			if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
			{
				Spawn();
			}
		}
	}
	#endregion MonoBehaviour

	#region Methods

	// Spawn object
	private void Spawn()
	{
		if (currentSpawned != null)
		{
			currentSpawned = Instantiate(ObjectToSpawn, SpawningZone, true);
			currentSpawned.SetActive(false);

			if (SpawnOnce)
			{
				spawned = true;
			}
		}
	}

	// Check on height, to be sure that hit plane is the lower
	public bool HeightOk(float height)
	{
		if(height<=minHeight)
		{
			minHeight = height;
			return true;
		}
		else
		{
			if(Mathf.Abs(height-minHeight)<=0.2f)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	// Check if raycast hit a plane
	private void Raycast()
	{
		// Create raycast hit list and feed it
		List<ARRaycastHit> hits = new List<ARRaycastHit>();
		raycastManager.Raycast(new Vector2(Screen.width / 2f, Screen.height / 2f), hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon);

		bool raycastOk = false;

		// If at least one contact point exists
		if (hits.Count > 0)
		{
			if(onlyGround) // Only ground option, will trigger heigh checking
			{
				if(HeightOk(hits[0].pose.position.y))
				{
					raycastOk = true;
				}
				else
				{
					raycastOk = false;
				}
			}
			else
			{
				raycastOk = true;
			}
		}
		else // No hit
		{
			raycastOk = false;
		}

		if (raycastOk)
		{
			// update transform infos
			currentSpawned.transform.position = hits[0].pose.position;
			currentSpawned.transform.rotation = hits[0].pose.rotation;

			// activate object if disabled
			if (!currentSpawned.gameObject.activeInHierarchy)
			{
				currentSpawned.gameObject.SetActive(true);
			}
		}
		else
		{
			// Disable object if active
			if (currentSpawned.gameObject.activeInHierarchy)
			{
				currentSpawned.gameObject.SetActive(false);
			}
		}
	}

	#endregion Methods
}

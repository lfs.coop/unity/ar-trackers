***This package was developped to propose tracking behaviours for AR (ARFoundation) Unity projects.***

**The different behaviours allow to use :**

- A Static Tracker (ST) : position won't be updated

- A Dynamic Tracker (DT) : position/rotation updated at track

- Correct the position when usage multiple ST.

- Unload a list of scenes / load a list of scenes at track (for ST & DT) using Additive of Single  
If using Single, bypassing unload/load asynchronously

- No trackers : usage of ProjectWithoutTracker script, allowing to detect planes through camera flux,  
raycast over it and place desired object at intersection point.


**The basics for correct usage are :**

- Tracker script (Tracker.cs) : Describe how the Tracker is parametered.  
At start, Tracker script will communicate to the TrackingManager the instance with the correct values.

- Tracking Manager script (TrackingManager.cs) : To place (at root, outside of Custome AR Session Origin),  
in every scene to provide Trackers behaviour. Getting all information at start about trackers.

- Custom AR Session Origin (prefab) : Usage of a custom AR Session Origin prefab. Mainly used when load additive  
to reset tracking state and tracked images.

- Project Without Tracker (ProjectWithoutTracker.cs) : This script allows to raycast over detected planes  
and place the desired object on intersection point.

**Warning : Activate ARPlaneManager / ARRaycastManager on Custom AR Session Origin to make it work.**


Detailed parameters :

- Tracker :
 - Tracking (**enum**) : values Static or Dynamic
 - Image Name (**sting**) : corresponding name for associated image
 - Correcting Position (**bool**) : specify if the tracker will correct the position
 - Scene Loading (**bool**) : specify if the tracker will trigger scene loading
 - Loading Mode (**LoadSceneMode**) : Single or Additive
 - ScenesToLoad (**string[]**) : array of scenes to load
 - ScenesToUnload (**string[]**) : array of scenes to unload
 - Instantiated (**bool**) : specify if object to affect exists or if we need to spawn it
 - Object To Move (**GameObject**): Reference to the object to move. Can be a prefab if instantiated.
 - Time Out (**float**) : How many seconds before time out object. Disabled if equal to 0s.
 - Time Out Forever (**bool**) : Is the object is timed out definitely ? (WIP)
 - Destroy Object After Timeout (**bool**) : Is the time out destroy the object ? (WIP)
 - Stop Render When Lost (**bool**) : If set to true, compute global object bounding box and check   
 if extremities points are seen by camera. 
 
- TrackingManager :
 - Minimum Number of Frames (**int**) : Minimum number of frame for position analysis at tracking.
 - Maximum Frame Distance (**float**) : Maximum allowed distance between 2 frames to achieve analysis 
 - Security time (**float**) : Security waiting timer before the first tracking and between different trackings.
 - Min Position Distance (**float**) : Minimum distance to trigger a Tracking
 - Max Position Distance (**float**) : Distance to exceed after correcting position to trigger the same tracker.
 - Min Position Time (**float**) : Minimum time value for the position correct
 - Max Position Time (**float**) : Maximum time value for the position correct
 - Correct Position Ready (**bool**) : False until one static Tracker was found
 - AR Session Origin (**GameObject**) : Reference to the ARSession Origin prefab in case of reset
 
- ProjectWithoutTracker :
 - Spawning Zone (**Transform**) : Transform parent to spawned object. If null, will spawn at root.
 - Object To Spawn (**GameObject**) : Prefab reference to spawn
 - Spawn Once (**bool**) : If true, spawn one time only
 - Spawned (**bool**) : Set to true at spawning.
 - Only Ground (**bool**) : If true, the raycast will constraint to lower planes.
 
- CanvasManager : 
 - Debugging (bool) : Enable or disable debugging mode. If activated, DebugPanel is enabled  
 and we can use the custom debug from anyway using **CanvasManager.instance.DebugLine** to *Debug.Log*  
 or **CanvasManager.instance.DebugWarning** to *Debug.LogWarning*.
 - Tracking Panel (**GameObject**) : Panel including text and filled image for tracking.
 - Tracking Image (**Image**) : Filled image for tracking.
 - Fade Background (**Image**): Fullscreen size image, lerping color on it to fade in / fade out.
 - Debug Panel (**Transform**) : Panel including scroll rect for manual debugging.
 - Content Root (**Transform**) : Reference transform for spawning Line prefabs.
 - Line (**GameObject**) : DebugLine prefab used for manual debugging.
 - C Start (**color**) : Start color for tracking indicator.
 - C End (**color**) : End color for tracking indicator.
 - Fade ON (**color**) : Color fade on, should be black.
 - Fade OFF (**color**) : Color fade off, should be transparent black.
 - Fade Time (**float**) : Length of fade in seconds
 
*The code of this package is provided under free software licence*